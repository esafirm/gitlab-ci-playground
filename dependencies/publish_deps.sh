#!/usr/bin/env bash

FILE=VERSION.txt
if [ -f "$FILE" ]; then
    echo "No changes in dependencies definition"
else
    echo "There's a change in dependencies definition."
    echo "Publishing deps"
    
    ## create this file with any content will works
    git hash-object dependencies/definition.txt > $FILE
    cat $FILE
fi